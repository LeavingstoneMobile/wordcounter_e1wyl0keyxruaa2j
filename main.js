const fs = require('fs')
const { StringDecoder } = require('string_decoder')
const decoder = new StringDecoder('utf8')

const fileContentsAsByteArray = fs.readFileSync("chapter.txt")

var fileTextAsUTF8CharArray = decoder.write(fileContentsAsByteArray)
const fileTextLength = fileTextAsUTF8CharArray.length

var charactersToWordsMap = new Uint32Array(fileTextLength)

var wordIndex = 0

var millisecondsBeforeStart = new Date().getTime()
var character = fileTextAsUTF8CharArray[0]

var nonReadableCharacterEncountered = true // true-თი ვიწყებთ იმ შემთხვევისთვის თუ პირველივე სიმბოლო არის non-readable
var characterIsNonreadable = false

const NOT_A_WORD = -1
var parsingHTMLtag = false

function imageMeta(fromWord, text) { return { fromWord: fromWord, text: text } }
var images = []
var imageTagText = ""
var wordBeforeImage = -1

for (var characterIndex = 0; characterIndex < fileTextLength; characterIndex++) {
    character = fileTextAsUTF8CharArray[characterIndex]


    /* HTML tags parsing */ {

        if (parsingHTMLtag == false
            && character == "<"
            && fileTextAsUTF8CharArray.substr(characterIndex, 5) == "<img ") {

            wordBeforeImage = wordIndex
            parsingHTMLtag = true
            wordIndex = wordIndex + 1
            nonReadableCharacterEncountered = false
        }

        if (parsingHTMLtag == true) {
            imageTagText += character
        }        

        if (parsingHTMLtag == true
            && character == ">"
            && fileTextAsUTF8CharArray.substr(characterIndex - 1, 2) == "/>") {
            parsingHTMLtag = false
            images.push(imageMeta(wordBeforeImage, imageTagText))
            imageTagText = ""
        }
    }

    if (parsingHTMLtag == false /*  თუ HTML ტეგის პარსვაზე ვართ მაშინ ვაჩერებთ არა კითხვად სიმბოლოებზე დაყოფას სიტყვებად */
        && (character == " " || character == "\n" /*და ასე შემდეგ სიმბოლოები*/)) {
        wordIndex = nonReadableCharacterEncountered == true ? wordIndex : wordIndex + 1
        nonReadableCharacterEncountered = true

        charactersToWordsMap[characterIndex] = NOT_A_WORD
    } else {
        nonReadableCharacterEncountered = false
        charactersToWordsMap[characterIndex] = wordIndex
    }

}

const millisecondsAfterStart = new Date().getTime()

const timeRequired = millisecondsAfterStart - millisecondsBeforeStart

console.log(`Character count: ${fileTextLength}`)
console.log(`Time required (seconds): ${timeRequired / 1000}`)
console.log(`Word count: ${wordIndex}`)
console.log(`Images: ${JSON.stringify(images)}`)

