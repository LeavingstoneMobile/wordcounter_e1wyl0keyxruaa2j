//
//  main.c
//  WordCounter
//
// ფაილი უნდა დაკომპილირედეს ოპტიმიზაციით: clang -O3 main.c
//
//  Created by Ivane on 3/14/18.
//  Copyright © 2018 Ivane. All rights reserved.
//

#include <stdio.h>
#include <time.h>
#include <string.h>
#include "Chapter.h"

int main(int argc, const char * argv[]) {
    char * ch = chapter1;
    // insert code here...
    
    unsigned long characterCount = strlen(ch);
    int wordCount = 0;
    
    struct timespec tsBefore;
    struct timespec tsAfter;
    
    clock_gettime(CLOCK_MONOTONIC, &tsBefore);
    
    for(int charIndex =0; charIndex < characterCount; charIndex++){
        char character = ch[charIndex];
        
        char unreadableCharacter1 = ' ';
        char unreadableCharacter2 = '\n';
        
        int isUndearableChar = character == unreadableCharacter1 || character == unreadableCharacter2;
        
        wordCount = wordCount + isUndearableChar;
    }
    
    clock_gettime(CLOCK_MONOTONIC, &tsAfter);
    
    unsigned long nanoseconds = tsAfter.tv_nsec - tsBefore.tv_nsec;
    
    printf("Time required in nanoseconds: %lu \n", nanoseconds);
    printf("Time required in seconds: %f \n", nanoseconds/1000000000.0);
    printf("Word count: %d\n", wordCount);
    printf("Char count: %lu\n", characterCount);
    
    return 0;
}
